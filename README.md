## TK1 - BangunRuma
[![pipeline status](https://gitlab.com/tk1-ppw-b12/tk1_new/badges/master/pipeline.svg)](https://gitlab.com/tk1-ppw-b12/tk1_new/commits/master)

**KELOMPOK KB-12
KELAS B**


## Anggota Kelompok:
- Gilbert Stefano Wijaya - 1806205584
- Ilma Ainur Rohma - 1806141220
- Nabila Azzahra - 1806205174
- Fauzan Pradana Linggih - 1806205281

## Link Herokuapp:
https://b12-tk1.herokuapp.com


## Deskripsi Website:
Web ini dibuat dengan latar belakang ketika ada orang yang ingin membangun rumah tetapi sulit menemukan kontraktor atau pekerja untuk membangun rumahnya. web ini hadir untuk menghubungan orang yang ingin membangun rumah dan kontraktor atau pekerja. Orang yang ingin membangun rumah atau user akan mengisi form request dengan ketentuan yang ada, kemudian ia menunggu sampai ada kontraktor yang ingin membangun rumahnya. Dari sisi kontraktor akan diberikan list user request(available project) beserta ketentuannya, ia tinggal memilih ingin mengerjakan proyek yang mana dengan mengisi form untuk hasil apa saja yang sudah mereka kerjakan yang ada pula. Web ini akan ada mitra kontraktor untuk melayanin user dengan cepat, namun kontraktor indie atau kontraktor kecil dapat juga menggunakan web ini untuk mencari proyek. Kedepannya web seperti ini diharapkan mampu terintegrasi dengan suatu tekonologi yang maju seperti 3D printer ataupun pengintegrasian lainnya, maka User hanya tinggal mengisi ketentuan yang ada lalu langsung proses membangun rumah.

## Daftar Fitur dan Pembagian Tugas:
- Nabila Azzahra - **Home Page** landing page berisi tentang web kita, dan juga form contact us 
- Fauzan Pradana Linggih - **Request Page** (User side) berisi form untuk request membangun rumah serta ditampilkan pula proyek proyek yang sudah jadi
- Gilbert Stefano Wijaya - **Contractor Page** (Contractor side) berisi tampilan available proyek, dan form untuk detail proyek
- Ilma Ainur Rohma - **Review Page** (Terdapat form yang berisi nama email, rating dan komen pengguna untuk aplikasi, komentar komentar yang sudah ada juga di tampilkan dalam halaman ini)
