from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import ReviewForm
from .models import Review
import datetime

def rev(request):
	data = Review.objects.all()
	form = ReviewForm(request.POST)
	content = {'form' : form, 'data' : data}
	return render(request, 'review.html', content)

def add_review(request):
	if request.method == 'POST':
		form = ReviewForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/review/')
	else:
		return redirect('/review/')



