from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'review'
urlpatterns = [
    path('', rev, name='rev'),
    path('add_review/', add_review, name='add_review')
]
