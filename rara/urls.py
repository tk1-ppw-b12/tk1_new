from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'rara'
urlpatterns = [
    path('', welcome, name='welcome'),
    path('save_quest/', save_Question, name='save_q')
    
]
