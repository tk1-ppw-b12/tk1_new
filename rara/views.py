from django.shortcuts import render, redirect
from .forms import QuestionForm
from .models import Question

# Create your views here.
def welcome(request):
    data = Question.objects.all()
    form = QuestionForm(request.POST)
    content = {'form' : form, 'data' : data}
    return render(request, "home.html", content)

def save_Question(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_Valid():
            form.save()
            return redirect('')
    else:
        return redirect('')