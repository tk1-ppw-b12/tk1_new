from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'index'
urlpatterns = [
    path('hello/', hello, name='hello'),
]
