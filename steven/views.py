from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import ProjectForm
from .models import Contractor
from dana.models import Request

def project(request):
    data = Request.objects.all()
    form = ProjectForm(request.POST)
    content = {'form' : form, 'data' : data}
    return render(request, 'steven.html', content)

def save_project(request):
	if request.method == 'POST':
		form = ProjectForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/contractor/')
	else:
		return redirect('/contractor/')

def rq_delete(request,id):
    rq = Request.objects.filter(id=id)
    rq.delete()
    return redirect('steven:project')
