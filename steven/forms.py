from django import forms
from .models import Contractor
from django.forms import ModelForm

class ProjectForm(ModelForm):
    class Meta:
        model= Contractor
        fields = ['name', 'email', 'project', 'link', 'detail']
        labels = {
            'name': 'Contractor Name','email':'Email', 'project':'Project', 'link':'Link', 'detail': 'Detail Project'
        }
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Contractor Name'}),
            'email' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'email',
                                        'placeholder' : 'Contractor\'s email?'}),
            'project' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'What\'s project?'}),
            'link' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'What\'s your drive link?'}),
            'detail' : forms.Textarea(attrs={'class': 'form-control',
                                        'type' : 'textarea', 'rows' : '3', 'placeholder' : 'Describe your project'}),
        }
