# Generated by Django 2.1.1 on 2019-10-18 07:23

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contractor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('project', models.CharField(max_length=50)),
                ('link', models.CharField(max_length=50)),
                ('detail', models.TextField()),
            ],
        ),
    ]
