from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Contractor
from django.utils import timezone


class ReviewTest(TestCase):
	def test_review_url_is_exist(self):
		response = Client().get('/contractor/')
		self.assertEqual(response.status_code,200)

	def test_review_using_review_template(self):
		response = Client().get('/contractor/')
		self.assertTemplateUsed(response, 'steven.html')

	def test_review_using_view_rev_func(self):
		found = resolve('/contractor/')
		self.assertEqual(found.func, project)

	def test_model_create_new_review(self):
		new_review = Contractor.objects.create(date=timezone.now(), name='Gilbert Stefano', email='gilbert', project='hello', link='google.com', detail='woy')
		counting_review_object = Contractor.objects.all().count()
		self.assertEqual(counting_review_object, 1)
