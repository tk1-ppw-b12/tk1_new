from django.db import models
import datetime

# Create your models here.

#RATE=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')]
RANGE =[('100-300 M','100-300 M'),('301-500 M','301-500 M'),('501-1000 M','501-1000 M'),('1001-2000 M','1001-2000 M'),('2001++ M','2001++ M')]

class Request(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    wideScale = models.CharField(max_length=50)
    budget = models.CharField(max_length=50, choices=RANGE)
    detail = models.TextField()
