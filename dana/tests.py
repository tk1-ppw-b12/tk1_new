from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Request
from django.utils import timezone


class RequestTest(TestCase):
	def test_request_url_is_exist(self):
		response = Client().get('/request/')
		self.assertEqual(response.status_code,200)

	def test_request_using_request_template(self):
		response = Client().get('/request/')
		self.assertTemplateUsed(response, 'request.html')

	def test_request_using_view_rev_func(self):
		found = resolve('/request/')
		self.assertEqual(found.func, req)

	def test_model_create_new_request(self):
		new_request = Request.objects.create(date=timezone.now(), name='Ilma Ainur Rohma', email='ilmarohma', wideScale = '100 m2', budget='100-300 M', detail='keren banget euy')
		counting_request_object = Request.objects.all().count()
		self.assertEqual(counting_request_object, 1)

	
     
